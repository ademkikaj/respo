// function to get all competences
var get_all_competence_types = function() {
    url = "http://164.68.109.183:8889/competencetype/all"
    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456')
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(res) {
            $.each(res, function(index, option) {
                var o = new Option(option.name, option.id);
                $(o).html(option.name);
                $("#competence_select").append(o);
            });
        },
        error: function(res) {
            console.log(res);
        }
    });
};


// functions for all listeners

// main function
var document_listeners = function() {

    // craete competence
    $("#create_competence").submit(function(event) {
        event.preventDefault();
        console.log("HELLO WORLD");

        // only gets the first three inputs
        form_data = $(this).serializeArray();
        console.log(form_data);

        selected_competence_type = $("#competence_select").val();
        if (selected_competence_type.length > 0) {
            data = {
                name_slo: form_data[0].value,
                name_eng: form_data[1].value,
                description: form_data[2].value,
                competence_type_id: { id: selected_competence_type }
            };
            console.log(data);
            data = JSON.stringify(data);
            url = "http://164.68.109.183:8889/competence/add";
            dataType = "JSON";
            $.ajax({
                url: url,
                type: 'POST',
                headers: {
                    "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456')
                },
                data: data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(res) {
                    console.log(res);
                },
                error: function(res) {
                    console.log(res);
                }
            });
        } else {
            console.log("didint select competence type");
        }

    });

    // create a competence type
    $("#create_competence_type").submit(function(event) {
        event.preventDefault();
        form_data = $(this).serializeArray();
        console.log(form_data[0].value)
        data = {
            "name": form_data[0].value,
            "description": form_data[1].value
        };

        url = "http://164.68.109.183:8889/competencetype/add"
        data = JSON.stringify(data);
        dataType = "JSON";
        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456')
            },
            data: data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(res) {
                console.log(res);
            },
            error: function(res) {
                console.log(res);
            }
        });
    });

};



// function to add competence type
$(document).ready(function() {

    // on load functions
    get_all_competence_types();
    document_listeners();


});