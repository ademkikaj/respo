// get all groups
var get_all_groups = function() {
    url = "http://localhost:8080/respogroup/all";
    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456')
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(res) {
            $.each(res, function(index, option) {
                // change to groups
                var o = new Option(option.name, option.id);
                $(o).html(option.name);
                $("#selectGroup").append(o);
            });
        },
        error: function(res) {
            console.log(res);
        }
    });

};
// add for search by group
var event_listeners = () => {

    // select change listener
    $("#selectGroup").change(function() {

        // display the users field
        if ($(this).val() == "") {
            $("#users_card").attr('hidden', true)
        } else {
            $("#users_card").attr('hidden', false)

            // get all users from a specific group
            url = "http://164.68.109.183:8889/userrespogroupcompetence/respogroup/" + $(this).val()
            $.ajax({
                url: url,
                type: 'GET',
                headers: {
                    "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456')
                },
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function(res) {
                    set_of_user_ids = new Set();
                    $.each(res, function(index, option) {
                        // get user id by group
                        set_of_user_ids.add(option.primaryKey.userId);
                    });
                    // TODO change this later to real url
                    url = "http://localhost:8080/respouser/"
                        // dictionary for users
                    var users = {};
                    // convert to array
                    set_of_user_ids = Array.from(set_of_user_ids);
                    for (user in set_of_user_ids) {
                        $.ajax({
                            url: url + set_of_user_ids[user],
                            type: 'GET',
                            headers: {
                                'Access-Control-Allow-Origin': '*',
                                "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456'),
                            },
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function(res) {
                                $.each(res, function(index, option) {
                                    // change to groups
                                    console.log(option);
                                    var o = new Option(option.firstName + " " + option.lastName, set_of_user_ids[user]);
                                    $(o).html(option.name);
                                    $("#selectUsers").append(o);
                                });
                            },
                            error: function(res) {
                                console.log(res);
                            }
                        });
                    }


                },
                error: function(res) {
                    console.log(res);
                }
            });
        }
    });

    // select user 
    $("#selectUsers").change(function() {
        // for testing purposes

        //id_of_user = $(this).val();
        id_of_user = 3
        url = "http://164.68.109.183:8889/analyse/" + id_of_user;
        $.ajax({
            url: url,
            type: 'GET',
            headers: {
                "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456')
            },
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(res) {
                console.log(res);
                algorithem = "Max Absolute Lack";
                $.each(res, function(index, option) {
                    console.log(option);
                    if (index == 1) {
                        algorithem = "Max relative Lack";
                    } else if (index == 2) {
                        algorithem = "Lack By Competence Relevance";
                    }
                    $("#statistics_body").append('<tr>' +
                        '<td>' +
                        '<div class="d-flex align-items-center">' +
                        '<div class="mr-3"></div>' +
                        '<div>' +
                        '<a class="text-default font-weight-semibold">' + option.nameOfCompetence + '</a>' +
                        '</div>' +
                        '</div>' +
                        '</td>' +
                        '<td><span class="badge bg-warning">Lacking</span></td>' +
                        '<td>' +
                        '<h6 class="font-weight-normal mb-0">' + algorithem + '</h6>' +
                        '</td>' +
                        '<td>' +
                        '<h6>' + option.value + '</h6>' +
                        '</td>' +
                        '<td>No training at this time</td>' +
                        '</tr>')

                });
            },
            error: function(res) {
                console.log(res);
            }
        });
    });
}




// function to add competence type
$(document).ready(function() {
    get_all_groups();
    event_listeners();
});