var get_all_groups_and_competences = function() {
    url = "http://164.68.109.183:8889/respogroupcompetence/all"
    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "Authorization": "Basic " + btoa('ademkikaj' + ":" + '123456')
        },
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(res) {
            const set_of_groups = new Set();
            var dict = new Object();
            console.log(res);
            $.each(res, function(index, option) {
                dict[option.respoGroup.name] = [];
                console.log(option.respoGroup);
                /* var o = new Option(option.name, option.id);
                $(o).html(option.name);
                $("#competence_select").append(o); */
            });

            $.each(res, function(index, option) {
                var competence = new Object();
                console.log(option);
                competence[option.competence.id] = option.competence.nameSlo;
                competence["reqScore"] = option.minReqScore;
                competence["relevance"] = option.relevance;
                dict[option.respoGroup.name].push(competence);
            });
            console.log(dict);
            for (group in dict) {
                const id_of_group = group.replace(" ", "");
                console.log(id_of_group);
                $("#main_container").append('<div class="card">' +
                    '<div class="card-header header-elements-inline">' +
                    '<h5 class="card-title">' + group + '</h5>' +
                    '<div class="header-elements">' +
                    '<div class="list-icons">' +
                    '<a class="list-icons-item" data-action="collapse"></a>' +
                    '<a class="list-icons-item" data-action="reload"></a>' +
                    '<a class="list-icons-item" data-action="remove"></a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +

                    '<table class="table datatable-basic">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>Competence id</th>' +
                    '<th>Competence name</th>' +
                    '<th>Required</th>' +
                    '<th>Relevance</th>' +
                    '<th class="text-center">Actions</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody id="' + id_of_group + '">' +
                    '</tbody>' +
                    '</table>' +
                    '</div>')
                var actualCounter = 0
                for (i in dict[group]) {
                    var counter = 0

                    for (j in dict[group][i]) {
                        if (counter == 0) {
                            $("#" + id_of_group).append('<tr id="' + id_of_group + actualCounter + '">' +
                                '<th>' + j + '</th>');
                            counter++;
                        }
                        $("#" + id_of_group + actualCounter).append('<th>' + dict[group][i][j] + '</th>')
                    }
                    $("#" + id_of_group + actualCounter).append('<th class="text-center">' +
                        '<a class="list-icons-item">X</a>' +
                        '<a class="list-icons-item" data-action="reload"></a>' +
                        '</th>')

                    console.log(actualCounter);
                    actualCounter++;
                }

            }


        },
        error: function(res) {
            console.log(res);
        }
    });
};

$(document).ready(function() {
    get_all_groups_and_competences();
});