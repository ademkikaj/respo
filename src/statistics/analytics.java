
// imports
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

// custom Object for competence
class Competence {
    public String name_of_competence = null; // name of competence
    public Integer value = 0; // value of competence
}

/*
 * Params: 
 * current_competence_grades = Dictionary of current employee
 * competences where <key> is unique identifier of a competence and <value> is the
 * value of that competence; 
 * competence_needed_grades = Dictionary of required
 * employee/student competences where <key> is unique identifier of a competence
 * and <value> is the value of that competence;
 * competence_importance = Dictionary of employee/student competences and their respective importance to the job;
 */

public class analytics {
    public static void main(String[] args) {

        // a few examples of use

        // initiate dictionaries
        Dictionary<String, Double> current_competence_grades = new Hashtable<String, Double>();
        Dictionary<String, Double> competence_needed_grades = new Hashtable<String, Double>();
        Dictionary<String, Double> competence_importance = new Hashtable<String, Double>();

        // set competences and values
        current_competence_grades.put("competence1", 80.0);
        current_competence_grades.put("competence2", 30.0);
        current_competence_grades.put("competence3", 45.0);

        competence_needed_grades.put("competence1", 60.0);
        competence_needed_grades.put("competence2", 60.0);
        competence_needed_grades.put("competence3", 60.0);

        competence_importance.put("competence1", 20.0);
        competence_importance.put("competence2", 30.0);
        competence_importance.put("competence3", 80.0);

        // calculate maximum absolute lack
        Competence worstCompetence = maximal_absolute_lack(current_competence_grades, competence_needed_grades);

        System.out.println("");
        System.out.println("*** Maximal absolute lack *** ");
        System.out.printf("Worst competence: %s with absolute lack: %d\n", worstCompetence.name_of_competence, worstCompetence.value);
        System.out.println("");

        // calculate maximum relative lack
        worstCompetence = maximal_relative_lack(current_competence_grades, competence_needed_grades);

        System.out.println("");
        System.out.println("*** Maximal relative lack *** ");
        System.out.printf("Worst competence: %s with relative lack: %d\n", worstCompetence.name_of_competence,
                worstCompetence.value);
        System.out.println("");

        worstCompetence = most_lacking_competence_by_importance(current_competence_grades, competence_needed_grades, competence_importance);

        System.out.println("");
        System.out.println("*** Maximal lacking competence by importance *** ");
        System.out.printf("Worst competence: %s with importance lack: %d \n", worstCompetence.name_of_competence, worstCompetence.value);
        System.out.println("");
    }

    // Method to calculate maximal absolute lack
    public static Competence maximal_absolute_lack(Dictionary<String, Double> current_competence_grades,
            Dictionary<String, Double> competence_needed_grades) {

        // initiate competence object
        Competence lacking_competence = new Competence();
        Double maximum_lack = 0.0;

        // get all competence ids and compare with current_competence_grades
        for (Enumeration i = competence_needed_grades.keys(); i.hasMoreElements();) {

            // get key of dictionary
            String key = i.nextElement().toString();

            // get values of current competence and required
            Double current_grade = current_competence_grades.get(key);
            Double needed_grade = competence_needed_grades.get(key);

            // calculate the absolute lack
            Double absolute_lack = needed_grade - current_grade;
            if (absolute_lack > 0 && absolute_lack > maximum_lack) {

                // add worst competence name and value to object
                lacking_competence.name_of_competence = key;
                lacking_competence.value = (int) Math.round(absolute_lack);
                maximum_lack = absolute_lack;
            }
        }
        return lacking_competence;
    }

    // method to calculate the maximal relative lack
    public static Competence maximal_relative_lack(Dictionary<String, Double> current_competence_grades,
            Dictionary<String, Double> competence_needed_grades) {

        // initiate competence object
        Competence lacking_competence = new Competence();
        Double maximum_lack = 0.0;

        // get all competence ids and compare with current_competence_grades
        for (Enumeration i = competence_needed_grades.keys(); i.hasMoreElements();) {

            // get key of dictionary
            String key = i.nextElement().toString();

            // get values of current competence and required
            Double current_grade = current_competence_grades.get(key);
            Double needed_grade = competence_needed_grades.get(key);

            // calculate relative lack
            Double relative_lack = 1 - (current_grade / needed_grade);
                
            if (relative_lack > 0 && relative_lack > maximum_lack) {

                // add worst competence name and value to object
                lacking_competence.name_of_competence = key;
                lacking_competence.value = (int) Math.round(relative_lack * 100);
                maximum_lack = relative_lack;
            }
        }
        return lacking_competence;
    }

    // method to calculate the lack by importance
    public static Competence most_lacking_competence_by_importance(Dictionary<String, Double> current_competence_grades,
            Dictionary<String, Double> competence_needed_grades, Dictionary<String, Double> competence_importance) {
        // initiate competence object
        Competence lacking_competence = new Competence();
        Double maximum_lack = 0.0;

        // get all competence ids and compare with current_competence_grades
        for (Enumeration i = competence_needed_grades.keys(); i.hasMoreElements();) {

            // get key of dictionary
            String key = i.nextElement().toString();

            // get values of current competence, required and importance
            Double current_grade = current_competence_grades.get(key);
            Double needed_grade = competence_needed_grades.get(key);
            Double importance = competence_importance.get(key);

            // calculate the lack by importance
            Double absolute_lack = needed_grade - current_grade;
            Double lack = (needed_grade - current_grade) * importance;
            if (absolute_lack > 0 && lack > maximum_lack) {

                // add worst competence name and value to object
                lacking_competence.name_of_competence = key;
                lacking_competence.value = (int) Math.round(lack);
                maximum_lack = lack;
            }

        }

        return lacking_competence;
    }

}