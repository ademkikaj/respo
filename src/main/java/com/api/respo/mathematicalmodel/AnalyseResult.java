package com.api.respo.mathematicalmodel;

public class AnalyseResult {

	private Competence maxAbsoluteLack;
	private Competence maxRelativeLack;
	private Competence mostLackingCompetenceByRelevance;

	public AnalyseResult(Competence maxAbsoluteLack, Competence maxRelativeLack,
			Competence mostLackingCompetenceByRelevance) {
		super();
		this.maxAbsoluteLack = maxAbsoluteLack;
		this.maxRelativeLack = maxRelativeLack;
		this.mostLackingCompetenceByRelevance = mostLackingCompetenceByRelevance;
	}

	public Competence getMaxAbsoluteLack() {
		return maxAbsoluteLack;
	}

	public void setMaxAbsoluteLack(Competence maxAbsoluteLack) {
		this.maxAbsoluteLack = maxAbsoluteLack;
	}

	public Competence getMaxRelativeLack() {
		return maxRelativeLack;
	}

	public void setMaxRelativeLack(Competence maxRelativeLack) {
		this.maxRelativeLack = maxRelativeLack;
	}

	public Competence getMostLackingCompetenceByRelevance() {
		return mostLackingCompetenceByRelevance;
	}

	public void setMostLackingCompetenceByRelevance(Competence mostLackingCompetenceByRelevance) {
		this.mostLackingCompetenceByRelevance = mostLackingCompetenceByRelevance;
	}

}
