package com.api.respo.mathematicalmodel;

import java.util.Dictionary;
import java.util.Enumeration;

/* 
 * @Author Matevz Ogrinc, Adem Kikaj
 * 
 * */

public class Model {

	// Method to calculate maximal absolute lack
	public Competence maximalAbsoluteLack(Dictionary<String, Competence> currentCompetenceScore, Dictionary<String, Competence> minimumRequiredScore) {

		// initiate competence object
		Competence lackingCompetence = new Competence();
		Double maximumLack = 0.0;

		// get all competence ids and compare with current_competence_grades
		for (Enumeration<String> i = minimumRequiredScore.keys(); i.hasMoreElements();) {

			// get key of dictionary
			String key = i.nextElement().toString();

			// get values of current competence and required
			Long competenceId = currentCompetenceScore.get(key).getCompetenceID();
			Double currentScore = (double) currentCompetenceScore.get(key).getValue();
			Double minimumRequiredS = (double) minimumRequiredScore.get(key).getValue();

			// calculate the absolute lack
			Double absoluteLack = minimumRequiredS - currentScore;
			if (absoluteLack > 0 && absoluteLack > maximumLack) {

				// add worst competence name and value to object
				lackingCompetence.setCompetenceID(competenceId);
				lackingCompetence.setNameOfCompetence(key);
				lackingCompetence.setValue((int) Math.round(absoluteLack));
				maximumLack = absoluteLack;
			}
		}
		return lackingCompetence;
	}

	// method to calculate the maximal relative lack
	public Competence maximalRelativeLack(Dictionary<String, Competence> currentCompetenceScore, Dictionary<String, Competence> minimumRequiredScore) {

		// initiate competence object
		Competence lackingCompetence = new Competence();
		Double maximumLack = 0.0;

		// get all competence ids and compare with current_competence_grades
		for (Enumeration<String> i = minimumRequiredScore.keys(); i.hasMoreElements();) {

			// get key of dictionary
			String key = i.nextElement().toString();

			// get values of current competence and required
			Long competenceId = currentCompetenceScore.get(key).getCompetenceID();
			Double currentScore = (double) currentCompetenceScore.get(key).getValue();
			Double minimumRequiredS = (double) minimumRequiredScore.get(key).getValue();

			// calculate relative lack
			Double relativeLack = 1 - (currentScore / minimumRequiredS);

			if (relativeLack > 0 && relativeLack > maximumLack) {

				// add worst competence name and value to object
				lackingCompetence.setCompetenceID(competenceId);
				lackingCompetence.setNameOfCompetence(key);
				lackingCompetence.setValue((int) Math.round(relativeLack * 100));
				maximumLack = relativeLack;
			}
		}
		return lackingCompetence;
	}

	// method to calculate the lack by importance
	public Competence mostLackingCompetenceByRelevance(Dictionary<String, Competence> currentCompetenceScore, Dictionary<String, Competence> minimumRequiredScore, Dictionary<String, Competence> competenceRelevance) {
		// initiate competence object
		Competence lackingCompetence = new Competence();
		Double maximumLack = 0.0;

		// get all competence ids and compare with current_competence_grades
		for (Enumeration<String> i = minimumRequiredScore.keys(); i.hasMoreElements();) {

			// get key of dictionary
			String key = i.nextElement().toString();

			// get values of current competence, required and importance
			Long competenceId = currentCompetenceScore.get(key).getCompetenceID();
			Double currentScore = (double) currentCompetenceScore.get(key).getValue();
			Double minimumRequiredS = (double) minimumRequiredScore.get(key).getValue();
			Double relevance = (double) competenceRelevance.get(key).getValue();

			// calculate the lack by importance
			Double absolute_lack = minimumRequiredS - currentScore;
			Double lack = (minimumRequiredS - currentScore) * relevance;
			if (absolute_lack > 0 && lack > maximumLack) {
				// add worst competence name and value to object
				lackingCompetence.setCompetenceID(competenceId);
				lackingCompetence.setNameOfCompetence(key);
				lackingCompetence.setValue((int) Math.round(lack));
				maximumLack = lack;
			}
		}
		return lackingCompetence;
	}
}
