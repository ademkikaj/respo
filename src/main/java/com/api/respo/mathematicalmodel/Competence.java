package com.api.respo.mathematicalmodel;

public class Competence {

	private Long competenceID;
	private String nameOfCompetence = null;
	private int value = 0;

	public Competence() {
		super();
	}

	public Competence(Long competenceID, String nameOfCompetence, int value) {
		super();
		this.competenceID = competenceID;
		this.nameOfCompetence = nameOfCompetence;
		this.value = value;
	}

	public Long getCompetenceID() {
		return competenceID;
	}

	public void setCompetenceID(Long competenceID) {
		this.competenceID = competenceID;
	}

	public String getNameOfCompetence() {
		return nameOfCompetence;
	}

	public void setNameOfCompetence(String nameOfCompetence) {
		this.nameOfCompetence = nameOfCompetence;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
