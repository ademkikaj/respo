package com.api.respo.service;

import org.springframework.stereotype.Service;

import com.api.respo.mathematicalmodel.Model;

@Service
public class MathematicalModelService {

	public Model getMathematicalModel() {
		return new Model();
	}
}
