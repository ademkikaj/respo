package com.api.respo.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.api.respo.model.User;
import com.api.respo.principal.UserPrincipal;
import com.api.respo.repo.UserRepository;

@Service
public class UserPrincipalDetailsService implements UserDetailsService{
	
	private UserRepository userRepo;
	
	public UserPrincipalDetailsService(UserRepository userRepo) {
		super();
		this.userRepo = userRepo;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.userRepo.findByUserName(username).get();
		if(user == null) {
	        throw new UsernameNotFoundException("Invalid User");
	    }
		UserPrincipal userPrincipal = new UserPrincipal(user);
		return userPrincipal;
	}

}
