package com.api.respo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.CompetenceType;
import com.api.respo.repo.CompetenceTypeRepository;

@CrossOrigin
@RestController
@RequestMapping("/competencetype")
public class CompetenceTypeController {

	@Autowired
	private CompetenceTypeRepository competenceTypeRepo;

	// GET Single

	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}")
	public ResponseEntity<CompetenceType> getSingleCompetenceType(@PathVariable Long id) throws RespoException {
		CompetenceType competenceType = competenceTypeRepo.findById(id)
				.orElseThrow(() -> new RespoException("Not found", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(competenceType);
	}

	// GET All
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/all")
	public List<CompetenceType> getAllCompetenceTypes() throws RespoException {
		return competenceTypeRepo.findAll();
	}

	// POST Create
	
	@Secured("ROLE_ADMIN")
	@PostMapping("/add")
	public ResponseEntity<CompetenceType> addCompetenceType(@Valid @RequestBody CompetenceType competenceType, Errors errors) throws RespoException {
		if(errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);
		try {
			competenceTypeRepo.save(competenceType);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage() + "", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(competenceType);
	}

	// PUT Update
	
	@Secured("ROLE_ADMIN")
	@PutMapping("/update")
	public ResponseEntity<CompetenceType> updateCompetenceType(@Valid @RequestBody CompetenceType competenceType, Errors errors) throws RespoException {
		competenceTypeRepo.findById(competenceType.getId()).orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		
		if(errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);
		
		try {
			competenceTypeRepo.save(competenceType);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage() + "", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(competenceType);
	}

	// DELETE Delete
	
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteCompetenceType(@PathVariable Long id) throws RespoException {
		CompetenceType competenceType = competenceTypeRepo.findById(id).orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		try {
			competenceTypeRepo.delete(competenceType);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage() + "", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().build();
	}

}
