package com.api.respo.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.User;
import com.api.respo.repo.UserMini;
import com.api.respo.repo.UserRepository;
import com.api.respo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("respouser")
public class UserController {

	@Autowired
	private UserRepository userRepo;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private UserService userService;

	// GET self
	@GetMapping()
	public ResponseEntity<User> getSelf(Principal principal) throws RespoException {
		User user = userRepo.findByUserName(principal.getName())
				.orElseThrow(() -> new RespoException("User not found!", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(user);
	}

	// GET Single
	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}")
	public ResponseEntity<UserMini> getUser(@PathVariable Long id) throws Exception {
		UserMini user = userRepo.findAllByIdAndOrganisation(id, userService.getUser().getOrganisation())
				.orElseThrow(() -> new RespoException("User not found!", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(user);
	}

	// GET All
	@Secured("ROLE_ADMIN")
	@GetMapping("/all")
	public List<UserMini> getAllUsers() {
		return userRepo.findAllByOrganisation(userService.getUser().getOrganisation());
	}

	// POST Add
	@Secured("ROLE_ADMIN")
	@PostMapping("/add")
	public ResponseEntity<User> addUser(@Valid @RequestBody User user, Errors errors) throws RespoException {
		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);
		try {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			userRepo.save(user);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage() + "", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(user);
	}

	// PUT Update

	// DELETE Delete

}
