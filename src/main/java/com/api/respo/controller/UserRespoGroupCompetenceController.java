package com.api.respo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.model.UserRespoGroupCompetence;
import com.api.respo.repo.UserRespoGroupCompetenceRepository;
import com.api.respo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("userrespogroupcompetence")
public class UserRespoGroupCompetenceController {

	@Autowired
	private UserRespoGroupCompetenceRepository userRespoGroupCompetenceRepo;
	@Autowired
	private UserService userService;
	
	// GET All by User
	@Secured("ROLE_ADMIN")
	@GetMapping("/competencies/{id}")
	public List<UserRespoGroupCompetence> getAllCompetenciesByUser(@PathVariable Long id) {
		return userRespoGroupCompetenceRepo.findAllByUserIdAndRespoGroupCompetenceRespoGroupOrganisation(id, userService.getUser().getOrganisation());
	}
	
}
