package com.api.respo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.RespoGroupCompetence;
import com.api.respo.repo.RespoGroupCompetenceRepository;
import com.api.respo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("respogroupcompetence")
public class RespoGroupCompetenceController {

	@Autowired
	private RespoGroupCompetenceRepository respoGroupCompetenceRepo;
	@Autowired
	private UserService userService;

	// GET All
	@Secured("ROLE_ADMIN")
	@GetMapping("/all")
	public List<RespoGroupCompetence> getAllRespoGroupCompetencies() {
		return respoGroupCompetenceRepo.findByRespoGroupOrganisation(userService.getUser().getOrganisation());
	}

	// GET Single

	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}")
	public ResponseEntity<RespoGroupCompetence> getSingleRespoGroupCompetence(@PathVariable Long id)
			throws RespoException {
		RespoGroupCompetence respoGroupCompetence = respoGroupCompetenceRepo.findByIdAndRespoGroupOrganisation(id, userService.getUser().getOrganisation())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(respoGroupCompetence);
	}
	
	// GET All Competencies by Respo Group ID
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/respogroup/{id}")
	public List<RespoGroupCompetence> getAllCompetenciesByRespoGroup(@PathVariable Long id){
		return respoGroupCompetenceRepo.findAllCompetenciesByRespoGroupIdAndRespoGroupOrganisation(id, userService.getUser().getOrganisation());
	}

	// POST Add

	@Secured("ROLE_ADMIN")
	@PostMapping("/add")
	public ResponseEntity<RespoGroupCompetence> addRespoGroupCompetence(
			@Valid @RequestBody RespoGroupCompetence respoGroupCompetence, Errors errors) throws RespoException {

		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
		try {
			respoGroupCompetenceRepo.save(respoGroupCompetence);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return ResponseEntity.ok().body(respoGroupCompetence);
	}

	// PUT Update

	@Secured("ROLE_ADMIN")
	@PutMapping("/update")
	public ResponseEntity<RespoGroupCompetence> updateRespoGroupCompetence(
			@Valid @RequestBody RespoGroupCompetence respoGroupCompetence, Errors errors) throws RespoException {

		respoGroupCompetenceRepo.findById(respoGroupCompetence.getId())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));

		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
		try {
			respoGroupCompetenceRepo.save(respoGroupCompetence);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return ResponseEntity.ok().body(respoGroupCompetence);
	}

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteRespoGroupCompetence(@PathVariable Long id) throws RespoException {
		RespoGroupCompetence respoGroupCompetence = respoGroupCompetenceRepo.findById(id)
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));

		try {
			respoGroupCompetenceRepo.delete(respoGroupCompetence);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return ResponseEntity.ok().build();
	}
}
