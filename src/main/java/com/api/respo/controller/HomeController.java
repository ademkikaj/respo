package com.api.respo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.PlatformRole;
import com.api.respo.model.User;
import com.api.respo.repo.UserRepository;

@CrossOrigin
@RestController
public class HomeController {

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/roles")
	private List<PlatformRole> getPrincipalRoles() throws RespoException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return userRepository.findByUserName(authentication.getName()).get().getPlatformRoles();
	}
	
	@GetMapping("/myUser")
	private User myUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return userRepository.findByUserName(authentication.getName()).get();
	}
	
}
