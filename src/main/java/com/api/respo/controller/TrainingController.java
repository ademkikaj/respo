package com.api.respo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.Training;
import com.api.respo.repo.TrainingRepository;

@CrossOrigin
@RestController
@RequestMapping("training")
public class TrainingController {

	@Autowired
	private TrainingRepository trainingRepo;

	// GET All

	@Secured("ROLE_ADMIN")
	@GetMapping("/all")
	public List<Training> getAllTrainings() {
		return trainingRepo.findAll();
	}

	// GET Single

	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}")
	public ResponseEntity<Training> getOrganisation(@PathVariable Long id) throws RespoException {
		Training training = trainingRepo.findById(id)
				.orElseThrow(() -> new RespoException("Organisation not found.", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(training);
	}

	// POST Add

	@Secured("ROLE_ADMIN")
	@PostMapping("/add")
	public ResponseEntity<Training> addOrganisation(@Valid @RequestBody Training training, Errors errors)
			throws RespoException {
		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);
		try {
			trainingRepo.save(training);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(training);
	}

	// PUT Update

	@Secured("ROLE_ADMIN")
	@PutMapping("/update")
	public ResponseEntity<Training> updateOrganisation(@Valid @RequestBody Training training, Errors errors)
			throws RespoException {

		trainingRepo.findById(training.getId())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));

		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);

		try {
			trainingRepo.save(training);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(training);

	}

	// DELETE delete

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteOrganisation(@PathVariable Long id) throws RespoException {
		Training training = trainingRepo.findById(id)
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		try {
			trainingRepo.delete(training);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().build();
	}

}
