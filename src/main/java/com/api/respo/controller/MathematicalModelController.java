package com.api.respo.controller;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.mathematicalmodel.AnalyseResult;
import com.api.respo.mathematicalmodel.Competence;
import com.api.respo.model.UserRespoGroupCompetence;
import com.api.respo.repo.UserRespoGroupCompetenceRepository;
import com.api.respo.service.MathematicalModelService;
import com.api.respo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("analyse")
public class MathematicalModelController {

	@Autowired
	private MathematicalModelService mathematicalModelService;
	@Autowired
	private UserRespoGroupCompetenceRepository userRespoGroupCompetenceRepo;
	@Autowired
	private UserService userService;

	Dictionary<String, Competence> currentCompetenceScore;
	Dictionary<String, Competence> minimumRequiredScore;
	Dictionary<String, Competence> competenceRelevance;
	Competence worstCompetence;

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public AnalyseResult index(@PathVariable Long id) {
		
		List<UserRespoGroupCompetence> userRespoGroupCompetencies = userRespoGroupCompetenceRepo
				.findAllByUserIdAndRespoGroupCompetenceRespoGroupOrganisation(id,
						userService.getUser().getOrganisation());
		
		currentCompetenceScore = new Hashtable<>();
		minimumRequiredScore = new Hashtable<>();
		competenceRelevance = new Hashtable<>();

		for (UserRespoGroupCompetence userRespoGroupCompetence : userRespoGroupCompetencies) {
			String name = userRespoGroupCompetence.getRespoGroupCompetence().getCompetence().getNameSlo();
			Long competenceId = userRespoGroupCompetence.getRespoGroupCompetence().getCompetence().getId();
			int competenceScore = userRespoGroupCompetence.getCompetenceScore();
			int minReqScore = userRespoGroupCompetence.getRespoGroupCompetence().getMinReqScore();
			int relevance = userRespoGroupCompetence.getRespoGroupCompetence().getRelevance();
			
			currentCompetenceScore.put(userRespoGroupCompetence.getRespoGroupCompetence().getCompetence().getNameSlo(), new Competence(competenceId, name, competenceScore));
			minimumRequiredScore.put(userRespoGroupCompetence.getRespoGroupCompetence().getCompetence().getNameSlo(), new Competence(competenceId, name, minReqScore));
			competenceRelevance.put(userRespoGroupCompetence.getRespoGroupCompetence().getCompetence().getNameSlo(), new Competence(competenceId, name, relevance));
		}

		Competence mal = mathematicalModelService.getMathematicalModel().maximalAbsoluteLack(currentCompetenceScore, minimumRequiredScore);
		Competence mrl = mathematicalModelService.getMathematicalModel().maximalRelativeLack(currentCompetenceScore, minimumRequiredScore);
		Competence mlci = mathematicalModelService.getMathematicalModel().mostLackingCompetenceByRelevance(currentCompetenceScore, minimumRequiredScore, competenceRelevance);
		return new AnalyseResult(mal, mrl, mlci);
	}

}
