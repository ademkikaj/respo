package com.api.respo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.SubSection;
import com.api.respo.repo.SectionRepository;
import com.api.respo.repo.SubSectionRepository;
import com.api.respo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("subsection")
public class SubSectionController {

	@Autowired
	private SubSectionRepository subSectionRepo;
	@Autowired
	private SectionRepository sectionRepo;
	@Autowired
	private UserService userService;
	
	// GET All

	@GetMapping("/all")
	public List<SubSection> getAllSubSections() {
		return subSectionRepo.findBySectionOrganisation(userService.getUser().getOrganisation());
	}

	// GET Single

	@GetMapping("/{id}")
	public ResponseEntity<SubSection> getSingleSubSection(@PathVariable Long id) throws RespoException {
		SubSection subSection = subSectionRepo.findByIdAndSectionOrganisation(id, userService.getUser().getOrganisation())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(subSection);
	}

	// POST Add

	@Secured("ROLE_ADMIN")
	@PostMapping("/add")
	public ResponseEntity<SubSection> addOrganisation(@Valid @RequestBody SubSection subSection, Errors errors)
			throws RespoException {
		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);
		try {			
			if(sectionRepo.findById(subSection.getSection().getId()).get().getOrganisation().getId() == userService.getUser().getOrganisation().getId())
				subSectionRepo.save(subSection);
			else
				throw new RespoException("Unauthorized to add a subsection under Section ID: " + subSection.getSection().getId(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(subSection);
	}

	// PUT Update

	@Secured("ROLE_ADMIN")
	@PutMapping("/update")
	public ResponseEntity<SubSection> updateOrganisation(@Valid @RequestBody SubSection subSection, Errors errors)
			throws RespoException {

		subSectionRepo.findById(subSection.getId())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));

		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);

		try {
			if(sectionRepo.findById(subSection.getSection().getId()).get().getOrganisation().getId() == userService.getUser().getOrganisation().getId())
				subSectionRepo.save(subSection);
			else
				throw new RespoException("Unauthorized to add a subsection under Section ID: " + subSection.getSection().getId(), HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(subSection);

	}

	// DELETE Delete

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteOrganisation(@PathVariable Long id) throws RespoException {
		SubSection subSection = subSectionRepo.findById(id)
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		try {
			subSectionRepo.delete(subSection);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().build();
	}

}
