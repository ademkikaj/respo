package com.api.respo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.Competence;
import com.api.respo.repo.CompetenceRepository;

/**
 * This is the Rest Controller for the competence Entity. It allows Cross
 * Origin. Can be used by the route "/competence"
 * 
 * @author Adem Kikaj
 * @version 1.3
 * 
 */

@CrossOrigin
@RestController
@RequestMapping("/competence")
public class CompetenceController {

	@Autowired
	private CompetenceRepository competenceRepository;

	// GET Single

	/**
	 * This method is used to get a single competence by providing as a parameter
	 * the Competence ID. 
	 * 
	 * @param id Long
	 * @return ResponseEntity<Competence>
	 * @throws RespoException
	 */
	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}")
	public ResponseEntity<Competence> getSingleCompetence(@PathVariable Long id) throws RespoException {
		Competence competence = competenceRepository.findById(id)
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(competence);
	}

	// GET All

	@Secured("ROLE_ADMIN")
	@GetMapping("/all")
	public List<Competence> getAllCompetencies() {
		return competenceRepository.findAll();
	}

	// POST Create

	@Secured("ROLE_ADMIN")
	@PostMapping("/add")
	public ResponseEntity<Competence> addCompetence(@Valid @RequestBody Competence competence, Errors errors)
			throws RespoException {
		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);
		try {
			competenceRepository.save(competence);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage() + "", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(competence);
	}

	// PUT Update

	@Secured("ROLE_ADMIN")
	@PutMapping("/update")
	public ResponseEntity<Competence> updateCompetence(@Valid @RequestBody Competence competence, Errors errors)
			throws RespoException {
		competenceRepository.findById(competence.getId())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));

		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);

		try {
			competenceRepository.save(competence);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage() + "", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(competence);
	}

	// DELETE Delete

	@Secured("ROLE_ADMIN")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteCompetence(@PathVariable Long id) throws RespoException {
		Competence competence = competenceRepository.findById(id)
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		try {
			competenceRepository.delete(competence);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage() + "", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().build();
	}

}
