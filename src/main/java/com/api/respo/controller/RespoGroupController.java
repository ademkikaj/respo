package com.api.respo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.RespoGroup;
import com.api.respo.repo.RespoGroupRepository;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin
@RestController
@RequestMapping("respogroup")
public class RespoGroupController {

	@Autowired
	private RespoGroupRepository respoGroupRepo;

	// GET All

	@Secured("ROLE_ADMIN")
	@GetMapping("/all")
	public List<RespoGroup> getAllRespoGroups() {
		return respoGroupRepo.findAll();
	}

	// GET Single

	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}")
	public ResponseEntity<RespoGroup> getSingleRespoGroup(@PathVariable Long id) throws RespoException {
		RespoGroup group = respoGroupRepo.findById(id)
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(group);
	}

	// POST Add

	@Secured("ROLE_ADMIN")
	@PostMapping("/add")
	public ResponseEntity<RespoGroup> addRespoGroup(@Valid @RequestBody RespoGroup respoGroup, Errors errors)
			throws RespoException {

		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);

		try {
			respoGroupRepo.save(respoGroup);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return ResponseEntity.ok().body(respoGroup);

	}

	// PUT Update

	@Secured("ROLE_ADMIN")
	@PutMapping("/update")
	public ResponseEntity<RespoGroup> updateRespoGroup(@Valid @RequestBody RespoGroup respoGroup, Errors errors) throws RespoException {

		respoGroupRepo.findById(respoGroup.getId())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		
		if(errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage(), HttpStatus.BAD_REQUEST);
		
		try {
			respoGroupRepo.save(respoGroup);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return ResponseEntity.ok().body(respoGroup);

	}

	// DELETE Delete
	
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteRespoGroup(@PathVariable Long id) throws RespoException{
		RespoGroup respoGroup = respoGroupRepo.findById(id).orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		
		try {
			respoGroupRepo.delete(respoGroup);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return ResponseEntity.ok().build();
	}
	

}
