package com.api.respo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.Section;
import com.api.respo.repo.SectionRepository;
import com.api.respo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("section")
public class SectionController {

	@Autowired
	private SectionRepository sectionRepo;
	@Autowired
	private UserService userService;

	// GET All

	@GetMapping("/all")
	@Secured("ROLE_ADMIN")
	public List<Section> getAllSections() {
		return sectionRepo.findByOrganisation(userService.getUser().getOrganisation());
	}

	// GET Single

	@GetMapping("/{id}")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<Section> getSingleSection(@PathVariable Long id) throws RespoException {
		Section section = sectionRepo.findByIdAndOrganisation(id, userService.getUser().getOrganisation())
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		return ResponseEntity.ok().body(section);
	}

	// POST Add
	@PostMapping("/add")
	@Secured("ROLE_ADMIN")
	public ResponseEntity<Section> addSection(@Valid @RequestBody Section section, Errors errors)
			throws RespoException {
		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);
		try {
			section.setOrganisation(userService.getUser().getOrganisation());
			sectionRepo.save(section);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(section);
	}

	// PUT Update
	@Secured("ROLE_ADMIN")
	@PutMapping("/update")
	public ResponseEntity<Section> updateSection(@Valid @RequestBody Section section, Errors errors)
			throws RespoException {

		Section tmpSection = sectionRepo.findById(section.getId()).orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));

		if (errors.hasErrors())
			throw new RespoException(errors.getAllErrors().get(0).getDefaultMessage() + "", HttpStatus.BAD_REQUEST);

		try {
			section.setOrganisation(tmpSection.getOrganisation());
			sectionRepo.save(section);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().body(section);

	}

	// DELETE Delete
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteOrganisation(@PathVariable Long id) throws RespoException {
		Section section = sectionRepo.findById(id)
				.orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
		try {
			sectionRepo.delete(section);
		} catch (Exception e) {
			throw new RespoException(e.getLocalizedMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok().build();
	}

}
