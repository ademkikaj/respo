package com.api.respo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.respo.exception.RespoException;
import com.api.respo.model.OrganisationRole;
import com.api.respo.repo.OrganisationRoleRepository;
import com.api.respo.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("organisationrole")
public class OrganisationRoleController {

	@Autowired
	private OrganisationRoleRepository organisationRoleRepo;
	@Autowired
	private UserService userService;
	
	@GetMapping("/all")
	@Secured("ROLE_ADMIN")
	public List<OrganisationRole> getAllOrganisationRoles() {		
		return organisationRoleRepo.findByOrganisation(userService.getUser().getOrganisation());
	}
	
	@GetMapping("/{id}")
	@Secured("ROLE_ADMIN")
	public OrganisationRole getSingleOrganisationRole(@PathVariable Long id) throws RespoException {
		return organisationRoleRepo.findByIdAndOrganisation(id, userService.getUser().getOrganisation()).orElseThrow(() -> new RespoException("Not found.", HttpStatus.NOT_FOUND));
	}
}
