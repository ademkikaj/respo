package com.api.respo.exception;

import java.util.Date;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(RespoException.class)
	public ResponseEntity<?> respoException(RespoException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), String.valueOf(ex.getStatus().value()), ex.getStatus().name(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, ex.getStatus());
	}
}
