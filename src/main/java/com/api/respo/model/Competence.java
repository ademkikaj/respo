package com.api.respo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "competence")
public class Competence {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "name_slo")
	@NotBlank(message = "Competence names in the Slovenian language cannot be blank.")
	private String nameSlo;
	@Column(name = "name_eng")
	@NotBlank(message = "Competence names in the English language cannot be blank.")
	private String nameEng;
	@Column(name = "description")
	private String description;
	@ManyToOne
	@JoinColumn(name = "competence_type_id", nullable = false)
	@NotNull(message = "Competence needs to belong to a Competence Type.")
	private CompetenceType competenceType;
	@OneToMany(mappedBy = "competence")
	private Set<TrainingCompetence> trainingCompetences = new HashSet<>();

	public Competence() {
		super();
	}

	public Competence(Long id, String nameSlo, String nameEng, String description, CompetenceType competenceType,
			Set<TrainingCompetence> trainingCompetences) {
		super();
		this.id = id;
		this.nameSlo = nameSlo;
		this.nameEng = nameEng;
		this.description = description;
		this.competenceType = competenceType;
		this.trainingCompetences = trainingCompetences;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameSlo() {
		return nameSlo;
	}

	public void setNameSlo(String nameSlo) {
		this.nameSlo = nameSlo;
	}

	public String getNameEng() {
		return nameEng;
	}

	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CompetenceType getCompetenceType() {
		return competenceType;
	}

	public void setCompetenceType(CompetenceType competenceType) {
		this.competenceType = competenceType;
	}

	public Set<TrainingCompetence> getTrainingCompetences() {
		return trainingCompetences;
	}

	public void setTrainingCompetences(Set<TrainingCompetence> trainingCompetences) {
		this.trainingCompetences = trainingCompetences;
	}

}
