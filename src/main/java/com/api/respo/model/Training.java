package com.api.respo.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "training")
public class Training {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message = "Training name cannot be blank.")
	private String name;
	@NotBlank(message = "Contractor cannot be blank.")
	private String contractor;
	@NotBlank(message = "Price cannot be blank.")
	private int price;
	@Column(name = "date_from")
	@NotBlank(message = "Starting date cannot be blank.")
	private Date dateFrom;
	@Column(name = "date_to")
	@NotBlank(message = "End date cannot be blank.")
	private Date dateTo;
	@Column(name = "distance_training")
	@NotBlank(message = "Distance training cannot be blank.")
	private boolean distanceTraining;
	@Column(name = "pre_qualification")
	private String preQualification;
	private String certificate;
	private String comment;
	@OneToMany(mappedBy = "training")
	private Set<TrainingCompetence> trainingCompetences = new HashSet<>();

	public Training() {
		super();
	}

	public Training(Long id, String name, String contractor, int price, Date dateFrom, Date dateTo,
			boolean distanceTraining, String preQualification, String certificate, String comment,
			Set<TrainingCompetence> trainingCompetences) {
		super();
		this.id = id;
		this.name = name;
		this.contractor = contractor;
		this.price = price;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.distanceTraining = distanceTraining;
		this.preQualification = preQualification;
		this.certificate = certificate;
		this.comment = comment;
		this.trainingCompetences = trainingCompetences;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContractor() {
		return contractor;
	}

	public void setContractor(String contractor) {
		this.contractor = contractor;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public boolean isDistanceTraining() {
		return distanceTraining;
	}

	public void setDistanceTraining(boolean distanceTraining) {
		this.distanceTraining = distanceTraining;
	}

	public String getPreQualification() {
		return preQualification;
	}

	public void setPreQualification(String preQualification) {
		this.preQualification = preQualification;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Set<TrainingCompetence> getTrainingCompetences() {
		return trainingCompetences;
	}

	public void setTrainingCompetences(Set<TrainingCompetence> trainingCompetences) {
		this.trainingCompetences = trainingCompetences;
	}

}
