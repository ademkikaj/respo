package com.api.respo.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "first_name")
	@NotBlank(message = "First name cannot be blank.")
	private String firstName;
	@Column(name = "last_name")
	@NotBlank(message = "Last name cannot be blank.")
	private String lastName;
	@Column(name = "username")
	@NotBlank(message = "Username cannot be blank.")
	private String userName;
	@Column(name = "password")
	@NotBlank(message = "Password cannot be blank.")
	private String password;
	@Column(name = "image_path")
	private String imagePath;
	@Column(name = "phone")
	private String phone;
	@Column(name = "city")
	private String city;
	@Column(name = "country")
	private String country;
	@Column(name = "email")
	@NotBlank(message = "Email cannot be blank.")
	private String email;
	@ManyToOne
	@JoinColumn(name = "organisation_id", nullable = false)
	@NotNull(message = "Organisation cannot be null.")
	private Organisation organisation;
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "user_platform_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "platform_role_id"))
	@NotNull(message = "User needs to have at least one Platform Role.")
	private List<PlatformRole> platformRoles;
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
	@JoinTable(name = "user_organisation_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "organisation_role_id"))
	@NotNull(message = "User needs to have at least one Organisation Role.")
	private List<OrganisationRole> organisationRoles;
	@OneToMany(mappedBy = "user")
	private Set<UserRespoGroupCompetence> userRespoGroupCompetences = new HashSet<>();

	public User() {
		super();
	}

	public User(long id, @NotBlank String firstName, String lastName, String userName, String password,
			String imagePath, String phone, String city, String country, String email, Organisation organisation,
			List<PlatformRole> platformRoles, List<OrganisationRole> organisationRoles) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.password = password;
		this.imagePath = imagePath;
		this.phone = phone;
		this.city = city;
		this.country = country;
		this.email = email;
		this.organisation = organisation;
		this.platformRoles = platformRoles;
		this.organisationRoles = organisationRoles;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<PlatformRole> getPlatformRoles() {
		return platformRoles;
	}

	public void setPlatformRoles(List<PlatformRole> platformRoles) {
		this.platformRoles = platformRoles;
	}

	public List<OrganisationRole> getOrganisationRoles() {
		return organisationRoles;
	}

	public void setOrganisationRoles(List<OrganisationRole> organisationRoles) {
		this.organisationRoles = organisationRoles;
	}

	public Set<UserRespoGroupCompetence> getUserRespoGroupCompetences() {
		return userRespoGroupCompetences;
	}

	public void setUserRespoGroupCompetences(Set<UserRespoGroupCompetence> userRespoGroupCompetences) {
		this.userRespoGroupCompetences = userRespoGroupCompetences;
	}
}
