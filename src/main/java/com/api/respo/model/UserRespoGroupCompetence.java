package com.api.respo.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.api.respo.embeddable.UserRespoGroupCompetenceId;

@Entity
@Table(name = "user_respo_group_competence")
public class UserRespoGroupCompetence {

	@EmbeddedId
	private UserRespoGroupCompetenceId primaryKey = new UserRespoGroupCompetenceId();

	@ManyToOne
	@MapsId("user_id")
	@JoinColumn(name = "user_id")
	private User user;
	@ManyToOne
	@MapsId("respo_group_competence_id")
	@JoinColumn(name = "respo_group_competence_id")
	private RespoGroupCompetence respoGroupCompetence;
	
	@Column(name = "competence_score")
	private int competenceScore;

	public UserRespoGroupCompetenceId getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(UserRespoGroupCompetenceId primaryKey) {
		this.primaryKey = primaryKey;
	}

	public int getCompetenceScore() {
		return competenceScore;
	}

	public void setCompetenceScore(int competenceScore) {
		this.competenceScore = competenceScore;
	}
	
	public RespoGroupCompetence getRespoGroupCompetence() {
		return respoGroupCompetence;
	}

	public void setRespoGroupCompetence(RespoGroupCompetence respoGroupCompetence) {
		this.respoGroupCompetence = respoGroupCompetence;
	}

}
