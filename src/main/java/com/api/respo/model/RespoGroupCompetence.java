package com.api.respo.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "respo_group_competence")
public class RespoGroupCompetence {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "respo_group_id", nullable = false)
	@NotNull(message = "Respo group cannot be null. Please provide an ID.")
	private RespoGroup respoGroup;
	@ManyToOne
	@JoinColumn(name = "competence_id", nullable = false)
	@NotNull(message = "Competence cannot be null. Please provide an ID.")
	private Competence competence;
	@Column(name = "min_req_score")
	@NotBlank(message = "Minimum required score cannot be blank.")
	private int minReqScore;
	@Column(name = "relevance")
	@NotBlank(message = "Minimum relevance cannot be blank.")
	private int relevance;
	@OneToMany(mappedBy = "respoGroupCompetence")
	private Set<UserRespoGroupCompetence> userRespoGroupCompetences = new HashSet<>();

	public RespoGroupCompetence() {
		super();
	}

	public RespoGroupCompetence(Long id, RespoGroup respoGroup, Competence competence, int minReqScore, int relevance) {
		super();
		this.id = id;
		this.respoGroup = respoGroup;
		this.competence = competence;
		this.minReqScore = minReqScore;
		this.relevance = relevance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RespoGroup getRespoGroup() {
		return respoGroup;
	}

	public void setRespoGroup(RespoGroup respoGroup) {
		this.respoGroup = respoGroup;
	}

	public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}

	public int getMinReqScore() {
		return minReqScore;
	}

	public void setMinReqScore(int minReqScore) {
		this.minReqScore = minReqScore;
	}

	public int getRelevance() {
		return relevance;
	}

	public void setRelevance(int relevance) {
		this.relevance = relevance;
	}

}
