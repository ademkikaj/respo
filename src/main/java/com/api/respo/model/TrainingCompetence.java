package com.api.respo.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.api.respo.embeddable.TrainingCompetenceId;

@Entity
@Table(name = "training_competence")
public class TrainingCompetence {

	@EmbeddedId
	private TrainingCompetenceId primaryKey = new TrainingCompetenceId();

	@ManyToOne
	@MapsId("training_id")
	@JoinColumn(name = "training_id")
	private Training training;
	@ManyToOne
	@MapsId("competence_id")
	@JoinColumn(name = "competence_id")
	private Competence competence;
	
	@Column(name = "evaluation_certainty")
	private int evaluationCertainty;
	@Column(name = "improvement_description")
	private String improvementDescription;
	private int improvement;

	public TrainingCompetenceId getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(TrainingCompetenceId primaryKey) {
		this.primaryKey = primaryKey;
	}

	public int getEvaluationCertainty() {
		return evaluationCertainty;
	}

	public void setEvaluationCertainty(int evaluationCertainty) {
		this.evaluationCertainty = evaluationCertainty;
	}

	public String getImprovementDescription() {
		return improvementDescription;
	}

	public void setImprovementDescription(String improvementDescription) {
		this.improvementDescription = improvementDescription;
	}

	public int getImprovement() {
		return improvement;
	}

	public void setImprovement(int improvement) {
		this.improvement = improvement;
	}

}
