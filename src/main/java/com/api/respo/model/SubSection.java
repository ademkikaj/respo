package com.api.respo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sub_section")
public class SubSection {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message = "Sub section name cannot be blank.")
	private String name;
	private String description;
	@ManyToOne
	@JoinColumn(name = "section_id")
	@NotNull(message = "Section can not be null.")
	private Section section;

	public SubSection() {
		super();
	}

	public SubSection(Long id, String name, String description, Section section) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.section = section;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	@Override
	public String toString() {
		return "SubSection [id=" + id + ", name=" + name + ", description=" + description + ", section=" + section
				+ "]";
	}

}
