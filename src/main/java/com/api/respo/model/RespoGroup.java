package com.api.respo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "respo_group")
public class RespoGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message = "Name of the Respo Group cannot be blank.")
	private String name;
	private String description;
	@ManyToOne
	@JoinColumn(name = "organisation_id", nullable = false)
	@NotNull(message = "Organisation cannot be null.")
	private Organisation organisation;

	public RespoGroup() {
		super();
	}

	public RespoGroup(Long id, @NotBlank(message = "Name of the Respo Group cannot be blank.") String name,
			String description, @NotNull(message = "Organisation cannot be null.") Organisation organisation) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.organisation = organisation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

}
