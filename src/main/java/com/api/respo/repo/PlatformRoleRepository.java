package com.api.respo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.PlatformRole;

public interface PlatformRoleRepository extends JpaRepository<PlatformRole, Long>{
	
}
