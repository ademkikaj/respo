package com.api.respo.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Organisation;
import com.api.respo.model.Section;

public interface SectionRepository extends JpaRepository<Section, Long>{

	List<Section> findByOrganisation(Organisation organisation);
	
	Optional<Section> findByIdAndOrganisation(Long id, Organisation organistion);
	
	
}
