package com.api.respo.repo;

public interface UserMini {
	String getFirstName();
	String getLastName();
	String getUserName();
	String getImagePath();
	String getPhone();
	String getCity();
	String getCountry();
	String getEmail();
}
