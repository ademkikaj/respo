package com.api.respo.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Organisation;
import com.api.respo.model.RespoGroupCompetence;

public interface RespoGroupCompetenceRepository extends JpaRepository<RespoGroupCompetence, Long>{

	List<RespoGroupCompetence> findAllCompetenciesByRespoGroupIdAndRespoGroupOrganisation(Long id, Organisation organisation);
	
	List<RespoGroupCompetence> findByRespoGroupOrganisation(Organisation organisation);
	
	Optional<RespoGroupCompetence> findByIdAndRespoGroupOrganisation(Long id, Organisation organisation);
}
