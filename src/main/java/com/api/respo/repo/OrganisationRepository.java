package com.api.respo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.respo.model.Organisation;

@Repository
public interface OrganisationRepository extends JpaRepository<Organisation, Long>{}
