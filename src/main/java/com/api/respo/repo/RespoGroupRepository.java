package com.api.respo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.RespoGroup;

public interface RespoGroupRepository extends JpaRepository<RespoGroup, Long>{

}
