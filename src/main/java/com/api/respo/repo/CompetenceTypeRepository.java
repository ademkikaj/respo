package com.api.respo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.CompetenceType;

public interface CompetenceTypeRepository extends JpaRepository<CompetenceType, Long> {

}
