package com.api.respo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Competence;

public interface CompetenceRepository extends JpaRepository<Competence, Long> {

}
