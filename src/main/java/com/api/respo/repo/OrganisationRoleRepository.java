package com.api.respo.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Organisation;
import com.api.respo.model.OrganisationRole;

public interface OrganisationRoleRepository extends JpaRepository<OrganisationRole, Long>{

	List<OrganisationRole> findByOrganisation(Organisation organisation);
	
	Optional<OrganisationRole> findByIdAndOrganisation(Long id, Organisation organisation);
}
