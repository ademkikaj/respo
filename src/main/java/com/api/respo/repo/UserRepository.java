package com.api.respo.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Organisation;
import com.api.respo.model.PlatformRole;
import com.api.respo.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	List<UserMini> findAllByOrganisation(Organisation organisation);
	
	Optional<User> findByUserName(String username);
	
	List<PlatformRole> findRolesByUserName(String username);

	Optional<UserMini> findAllById(Long id);
	
	Optional<UserMini> findAllByIdAndOrganisation(Long id, Organisation organisation);
}
