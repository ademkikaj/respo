package com.api.respo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Organisation;
import com.api.respo.model.UserRespoGroupCompetence;

public interface UserRespoGroupCompetenceRepository extends JpaRepository<UserRespoGroupCompetence, Long>{

	List<UserRespoGroupCompetence> findAllByUserIdAndRespoGroupCompetenceRespoGroupOrganisation(Long id, Organisation organisation);
	
}
