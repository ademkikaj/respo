package com.api.respo.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Organisation;
import com.api.respo.model.SubSection;

public interface SubSectionRepository extends JpaRepository<SubSection, Long>{

	List<SubSection> findBySectionOrganisation(Organisation organisation);
	
	Optional<SubSection> findByIdAndSectionOrganisation(Long id, Organisation organisation);
}
