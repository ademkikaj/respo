package com.api.respo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.respo.model.Training;

public interface TrainingRepository extends JpaRepository<Training, Long>{

}
