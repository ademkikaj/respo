package com.api.respo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RespoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RespoApiApplication.class, args);
	}

}
