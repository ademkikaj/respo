package com.api.respo.embeddable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrainingCompetenceId implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "training_id")
	private Long trainingId;
	@Column(name = "competence_id")
	private Long competenceId;
	
	public Long getTrainingId() {
		return trainingId;
	}
	public void setTrainingId(Long trainingId) {
		this.trainingId = trainingId;
	}
	public Long getCompetenceId() {
		return competenceId;
	}
	public void setCompetenceId(Long competenceId) {
		this.competenceId = competenceId;
	}
	

}
